# rbrowse

Use rofi, with a simple theme, as a very simple web search application. Support custom search engines and shortcuts.

## Getting Started

This steps will help you install rbrowse on your machine

### Dependency

rbrowser only need rofi to function, look [this page](https://github.com/davatorium/rofi/blob/next/INSTALL.md) to install it.

Debian or Ubuntu
``` bash
sudo apt install rofi
```

ArchLinux
``` bash
sudo pacman -S rofi
```

### Installing


To install rbrowser, clone the repo and use the controller script to install everything needed

``` bash
git clone https://gitlab.com/Yoann.truchy/rbrowse.git
cd rbrowse
./control install
```

### Usage

![usage gif](src/usage.gif)

To launch the script, open a terminal and type the program's name
``` bash
rbrowse
```

rbrowse supports custom search engines and shortcuts. These must be specified manually in the config file, it is located at `~/.config/rbrowse/config.yml`. The syntax for Search engine is the following:

``` yaml
<shortcut>: http://<search-engine>/query={}
```
Make sure to put `{}` where the query string should be.

The default search engine __must__ use DEFAULT as shortcut name. Here is the default config file, it uses google as default search engine, and is provided with some examples:

``` yaml
web_engines:
        DEFAULT: https://google.com/search?q={}
        yt: https://www.youtube.com/results?search_query={}

shortcuts:
        gitlab: https://gitlab.com
        nf: https://netflix.com/browse
```

To use custom entries, input the shortcut name, then every every thing past it will be put inside the `{}`.
## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
